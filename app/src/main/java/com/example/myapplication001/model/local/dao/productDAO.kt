package com.example.myapplication001.model.local.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.myapplication001.model.entities.Product


@Dao
interface productDAO {
    @Query("select * from products")
    fun getAll():List<Product>

    @Query("select * from products where `key`=:keyValue")
    fun getByKey(keyValue:Int):Product

    @Insert
    fun add(myProduct: Product)

    @Update
    fun update(myProduct: Product)

    @Delete
    fun delete(myProduct: Product)
}