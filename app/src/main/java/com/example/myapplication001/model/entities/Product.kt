package com.example.myapplication001.model.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.example.myapplication001.model.ProductStatus
import com.google.firebase.firestore.Exclude
import com.google.firebase.firestore.PropertyName
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


@Entity(tableName = "products")
data class Product(
    @PrimaryKey(autoGenerate = true)
    @JvmField @Exclude
    @Expose(serialize = false, deserialize = false)
    var key: Int? = null,
    @Ignore
    @JvmField @Exclude
    var id: String = "",
    var name: String = "",
    var price: Int = 0,
    @ColumnInfo(name = "url_image")
    @JvmField @PropertyName("url_image")
    @SerializedName("url_image")
    var urlImage:String = "https://e7.pngegg.com/pngimages/995/137/png-clipart-inventory-management-software-inventory-control-cartoon-inventory-management-service-public-relations-thumbnail.png",
    var description:String="",
    var status: ProductStatus = ProductStatus.AVAILABLE
):Serializable {


    init {
        //println("Producto creado: $name - $$price")
    }

    @Exclude
    fun getShortInfo(): String = "$name - $$price"

    override fun toString(): String {
        return "Product(name='$name', price=$price)"
    }

}