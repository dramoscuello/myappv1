package com.example.myapplication001.model.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.myapplication001.model.entities.Product
import com.example.myapplication001.model.local.dao.productDAO


@Database(entities = [Product::class], version = 1, exportSchema = true)
abstract class StoreAppDB:RoomDatabase() {
    abstract fun productDAO():productDAO


    companion object{
        private var INSTANCE:StoreAppDB? = null
        fun getInstance(myContext:Context):StoreAppDB{
            var instance = INSTANCE
            if(instance==null){
                instance = Room.databaseBuilder(myContext, StoreAppDB::class.java, "storeapp.db")
                    .allowMainThreadQueries().build()
            }

            return instance
        }
    }

}