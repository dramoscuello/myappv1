package com.example.myapplication001.view

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.myapplication001.R
import com.example.myapplication001.viewmodel.RegisterViewModel
import com.example.myapplication001.databinding.ActivityRegisterBinding
import com.example.myapplication001.model.entities.User

class Register : AppCompatActivity() {

    private lateinit var binding:ActivityRegisterBinding
    private lateinit var viewModel: RegisterViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_register)

        viewModel = ViewModelProvider(this)[RegisterViewModel::class.java]


        binding.viewmodel = viewModel


        binding.btnRegister.setOnClickListener {
            viewModel.singUp().observe(this) {
                it?.let {
                    print("SingUp")
                    login(it)
                }?:run {
                    Toast.makeText(applicationContext, getString(R.string.error_sign_up), Toast.LENGTH_SHORT).show()
                }
            }
        }

        binding.btnReset.setOnClickListener {
            finish()
        }
    }

    private fun login(user: User) {
        val preferences: SharedPreferences =
            getSharedPreferences("shad.pref", MODE_PRIVATE)
        val editor: SharedPreferences.Editor = preferences.edit()
        editor.putBoolean("login", true)
        editor.apply()

        val intentLogin = Intent(applicationContext, ProductListActivity::class.java)
        intentLogin.apply {
            putExtra("message", "Hola")
            putExtra("data", user.email)
            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        }
        startActivity(intentLogin)
        Toast.makeText(this, "Inciando Sesion....", Toast.LENGTH_SHORT).show()
    }
}