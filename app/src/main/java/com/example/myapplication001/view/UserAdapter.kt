package com.example.myapplication001.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication001.R

import com.example.myapplication001.databinding.UserItemBinding
import com.example.myapplication001.model.entities.RegisterUser

class UserAdapter(private var users:ArrayList<RegisterUser>):RecyclerView.Adapter<UserAdapter.UserViewHolder>(){

    fun refresh(myUsers: ArrayList<RegisterUser>){
        users = myUsers
        notifyDataSetChanged()
    }

    class UserViewHolder(val binding: UserItemBinding):RecyclerView.ViewHolder(binding.root){
        fun bind (myUser: RegisterUser){
            binding.user = myUser
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        val inflate = LayoutInflater.from(parent.context)
        val binding: UserItemBinding = DataBindingUtil.inflate(inflate,
            R.layout.user_item,
            parent,
            false )
        return UserViewHolder(binding)
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        holder.bind(users[position])
    }

    override fun getItemCount(): Int = users.size
}