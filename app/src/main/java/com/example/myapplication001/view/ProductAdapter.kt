package com.example.myapplication001.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication001.R
import com.example.myapplication001.databinding.ProductItemBinding
import com.example.myapplication001.model.entities.Product

class ProductAdapter(private var products:ArrayList<Product>):RecyclerView.Adapter<ProductAdapter.ProductViewHolder>() {

    var onItemClickListener:((Product)->Unit)?=null
    var onItemLongClickListener:((Product)->Unit)?=null



    fun refresh(myProducts: ArrayList<Product>){
        products = myProducts
        notifyDataSetChanged()


    }

    class ProductViewHolder(val binding: ProductItemBinding):RecyclerView.ViewHolder(binding.root){
        fun bind (myProduct: Product, onItemClickListener: ((Product) -> Unit)?, onItemLongClickListener: ((Product) -> Unit)?){
            binding.product = myProduct


            //Glide.with(binding.root.context).load(myProduct.urlImage).into(binding.imgProductItem)

            binding.root.setOnClickListener{
                onItemClickListener?.let{
                    it(myProduct)
                }
            }

            binding.root.setOnLongClickListener{
                onItemLongClickListener?.let {
                    it(myProduct)
                }
                true
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val inflate = LayoutInflater.from(parent.context)
        val binding: ProductItemBinding = DataBindingUtil.inflate(inflate,
            R.layout.product_item,
            parent,
            false )
        return ProductViewHolder(binding)

    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        holder.bind(products[position], onItemClickListener, onItemLongClickListener)
    }

    override fun getItemCount(): Int = products.size
}