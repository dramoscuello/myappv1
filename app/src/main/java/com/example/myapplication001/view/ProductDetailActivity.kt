package com.example.myapplication001.view

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.myapplication001.R
import com.example.myapplication001.databinding.ActivityProductDetailBinding
import com.example.myapplication001.model.entities.Product
import com.example.myapplication001.viewmodel.ProductDetailActivityViewModel

class ProductDetailActivity : AppCompatActivity() {

    lateinit var binding:ActivityProductDetailBinding
    lateinit var viewModel:ProductDetailActivityViewModel
    private var myProductKey:Int = 0
    private var myProductId:String? = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_product_detail)

        //me perdi en la ultima perte de la grabación del viernes 08

        myProductKey = intent.getIntExtra("product_key", 0)
        myProductId = intent.getStringExtra("product_id")

        binding = DataBindingUtil.setContentView(this, R.layout.activity_product_detail)
        viewModel = ViewModelProvider(this)[ProductDetailActivityViewModel::class.java]


        //viewModel.getProductByKey(myProductKey)
        myProductId?.let { viewModel.getProductById(it) }

        //viewModel.product = myProduct


        //binding.product = viewModel.product

        binding.product = Product(name = "", price = 0)

        viewModel.product.observe(this){
            it?.let {
                binding.product = it
            }


        }

        binding.btnEditProductDetail.setOnClickListener {
            val intentForm = Intent(applicationContext, ProductFormActivity::class.java)
            intentForm.putExtra("product", binding.product)

            startActivity(intentForm)

        }


        binding.btnReturnProductDetail.setOnClickListener {
            finish()
        }


    }

    override fun onResume() {
        //viewModel.getProductByKey(myProductKey)
        myProductId?.let { viewModel.getProductById(it) }
        //binding.product = viewModel.product
        super.onResume()
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.mi_menu -> {
                logout()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun logout() {
        val preferences: SharedPreferences = getSharedPreferences("shad.pref", MODE_PRIVATE)
        val editor = preferences.edit()
        editor.clear()
        editor.apply()

        val intent = Intent(applicationContext, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }
}