package com.example.myapplication001.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.myapplication001.R
import com.example.myapplication001.viewmodel.UserListActivityViewModel
import com.example.myapplication001.databinding.ActivityUserListBinding

class UserListActivity : AppCompatActivity() {

    lateinit var binding:ActivityUserListBinding
    lateinit var viewmodel: UserListActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        binding = DataBindingUtil.setContentView(this, R.layout.activity_user_list)

        viewmodel = ViewModelProvider(this)[UserListActivityViewModel::class.java]

        binding.viewModel = viewmodel

        viewmodel.loadUsers()
        viewmodel.refreshData()
    }
}