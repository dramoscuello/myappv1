package com.example.myapplication001.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.myapplication001.R

class KotlinActivity : AppCompatActivity() {

    lateinit var variable:String //lateinit nos permite inicializar una variable luego y no da error por la obligación de inicializarla enseguida

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //variables, constantes y tipos de datos

        // "var" se usa para variables y "val" para constantes

        var name:String="Deimer"
        var altura:Double=1.70
        var age:Int=29
        var admin:Boolean=false

        println("Mi nombre es $name")
        name="Laura"
        println("Mi nombre es $name")

        //Estructuras de control

        val email:String = getString(R.string.email)
        val pass:String = getString(R.string.pass)

        val emailInput = "dramos.cuello@gmail.com"
        val passInput = "deimer03"


        if(email == emailInput && pass == passInput){
            println("Iniciando sesión...")
        }else if(email != emailInput){
            println("Email no válido")
        }else if(pass != passInput){
            println("Password invalida")
        }

        //WHEN
        val typeUser = 1


        when(typeUser){
            1-> {
                println("Soy admin")
            }2->{
                println("Soy Cliente")
            }in 3..5 ->{
                println("Soy invitado")
            }else ->{
                println("Soy Sapo")
            }
        }

        //Estructuras de datos

        val products:ArrayList<String> = arrayListOf("Monitor", "Mouse", "Teclado")
        println(products)
        products.add("Diademas")
        println(products)

        val mouse = products[1]
        println(mouse)

        products[0] = "Pantalla"


        products.removeAt(3)

        //Ciclo forEach
        products.forEach{
            println(it)
        }

        // Diccionarios - mapas

        //val monitor = mapOf<String, Any>("name" to "Monitor")
        val monitor = mutableMapOf<String, Any>("name" to "Monitor") //permite modificar el mapa, en anterior no.
        monitor["price"] = 700000
        monitor["amount"] = 45
        println(monitor)

        monitor.remove("amount")

        //bucles

        for (product:String in products){
            println(product)
        }

        for (entry:Map.Entry<String, Any> in monitor){
            println("Clave: ${entry.key} - Valor: ${entry.value}")
        }


        //until se usa para no tomar el ultimo valor del rango
        // downTo hace el ciclo al revés
        for (i:Int in 0 until products.size step 1){
            println(products[i])
        }

        //While

        var x:Int = 0
        while (x<3){
            println(x)
            x++
        }

        //Do while
        var y:Int = 0
        do{
            y++
            println(y)
        }while (y<3)


        // Null safety
        // el signo ?  nos permite declarar variables nulas o que estas reciban valores nulos
        var testNull:String? = "null"

        val textSize:Int? = testNull?.length

        //El doble signo de admiración nos dice que el valor de la variable NUNCA será nulo
        val otherTextNull:Int = testNull!!.length

        // let ejecuta cuando los valores no son nulos
        textSize?.let {
            println(it)
        }?:run{
           println("El texto es nulo")
        }

        //Funciones
        loadProducts()
        showProduct("Disco duro", 24000)
        //println(login("dramos.cuello@gmail.com", "deimer03"))


        //Clases
        //val CProduct = Product("Portatil ASUS", 40000, "Bueno")
        //CProduct.getShortInfo()

        //Descoposición. En una tupla de variables se atrapan los valores obtenidos desde la clase
        //val (n, p, d, s) = CProduct


        //val myClient: Client = Client(name="dddddd", password = "jdsijiws")
        //myClient.login()

        //Lambdas
        clickListener {
            println(it)
            true
        }



    }

    //funciones

    //Unit es como Void, es decir, que la función no retornará valor
    fun loadProducts():Unit{
        /*
        * Obtener los productos
        *
        * */
    }

    fun showProduct(name: String, price: Any): Unit{
        println("El producto $name tiene un valor de $price")
    }

    /*fun login(email:String, pass: String):Boolean{
        return email == getString(R.string.email) && pass == getString(R.string.pass)
    }*/

    fun clickListener(click:(String)->Boolean){
        click("Hola")
    }
}