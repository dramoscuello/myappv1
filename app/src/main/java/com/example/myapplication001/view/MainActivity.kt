package com.example.myapplication001.view

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.myapplication001.viewmodel.MainActivityViewModel
import com.example.myapplication001.R
import com.example.myapplication001.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding:ActivityMainBinding
    private lateinit var viewModel: MainActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val preferences = getSharedPreferences("shad.pref", MODE_PRIVATE)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        viewModel = ViewModelProvider(this)[MainActivityViewModel::class.java]


        binding.viewModel = viewModel


        if (preferences.getBoolean("login", false) && preferences.getString("email","") != ""){
            goToProductList()
        }


        binding.btnRegister.setOnClickListener {
            val intentSignup = Intent(applicationContext, Register::class.java)
            startActivity(intentSignup)
        }

        binding.btnLogin.setOnClickListener {
            viewModel.login().observe(this) {
                it?.let {
                    login()

                } ?: run {
                    Toast.makeText(this, "Datos inválidos", Toast.LENGTH_SHORT).show()
                }
            }
        }

        //setContentView(R.layout.activity_main)
    }

    private fun login() {
        val preferences: SharedPreferences =
            getSharedPreferences("shad.pref", MODE_PRIVATE)
        val editor: SharedPreferences.Editor = preferences.edit()
        editor.putBoolean("login", true)
        editor.putString("email", viewModel.user.email)
        editor.apply()

        goToProductList()
        Toast.makeText(this, "Inciando Sesión...", Toast.LENGTH_SHORT).show()
    }

    private fun goToProductList() {
        val preferences: SharedPreferences =
            getSharedPreferences("shad.pref", MODE_PRIVATE)
        val intentLogin = Intent(applicationContext, ProductListActivity::class.java)
        intentLogin.apply {
            putExtra("message", "Hola")
            putExtra("data", preferences.getString("email", ""))
        }
        startActivity(intentLogin)
        finish()
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onRestart() {
        super.onRestart()
    }


}