package com.example.myapplication001.view

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.myapplication001.viewmodel.ProductListActivityViewModel
import com.example.myapplication001.R
import com.example.myapplication001.databinding.ActivityProductListBinding
import com.example.myapplication001.model.entities.Product

class ProductListActivity : AppCompatActivity() {

    lateinit var binding: ActivityProductListBinding
    lateinit var viewModel: ProductListActivityViewModel
    lateinit var adapter: ProductAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        var bundle:Bundle? = intent.extras

        //var message:String?=bundle?.getString("message")
        var message :String? = intent.getStringExtra("message")
        var data:String?=intent.getStringExtra("data")


        message?.let {
            title="$message $data"
        }


        //Grabacion de la noche



        binding = DataBindingUtil.setContentView(this, R.layout.activity_product_list)


        viewModel = ViewModelProvider(this)[ProductListActivityViewModel::class.java]


        adapter = ProductAdapter(arrayListOf())
        binding.adapter = adapter


        loadProducts()
        //viewModel.loadProducts()
        //viewModel.refreshData()

        adapter.onItemClickListener={
            //Toast.makeText(applicationContext, it.name, Toast.LENGTH_SHORT).show()

            val intentDetail = Intent(applicationContext, ProductDetailActivity::class.java)
            intentDetail.putExtra("product_key", it.key)
            intentDetail.putExtra("product_id", it.id)

            startActivity(intentDetail)
        }

        adapter.onItemLongClickListener={
            //viewModel.deleteProduct(it)
            //adapter.refresh(viewModel.products)
            viewModel.deleteProduct(it).observe(this) { state ->
                if (state) {
                    Toast.makeText(
                        applicationContext,
                        "Producto ${it.name} eliminado...",
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    Toast.makeText(applicationContext, "Error: ${it.name}", Toast.LENGTH_SHORT).show()
                }
            }
            Toast.makeText(applicationContext, "Producto ${it.description} eliminado...", Toast.LENGTH_SHORT).show()
        }

        binding.btnAdd.setOnClickListener {
            startActivity(Intent(applicationContext, ProductFormActivity::class.java))
        }
    }

    private fun loadProducts(){
        //viewModel.loadProducts()
        viewModel.products.observe(this){
            if(it.isEmpty()){
                viewModel.loadFakeData()
            }
            adapter.refresh(it as ArrayList<Product>)
        }
    }

    override fun onResume() {
        viewModel.loadProducts()
        super.onResume()


        //adapter.refresh(viewModel.products)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.mi_menu -> {
                logout()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun logout() {
        val preferences: SharedPreferences = getSharedPreferences("shad.pref", MODE_PRIVATE)
        val editor = preferences.edit()
        editor.clear()
        editor.apply()

        val intent = Intent(applicationContext, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }
}