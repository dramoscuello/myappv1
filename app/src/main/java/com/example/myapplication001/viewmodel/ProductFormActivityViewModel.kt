package com.example.myapplication001.viewmodel

import android.app.Application
import android.net.Uri
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.example.myapplication001.model.entities.Product
import com.example.myapplication001.model.repository.ProductRepository

class ProductFormActivityViewModel(application: Application):AndroidViewModel(application) {

    private val productRepository:ProductRepository = ProductRepository(application)

    var product = Product(name = "", price = 0)

    fun add(photoUri: Uri?): LiveData<String> {
        //productRepository.addLocal(product)
        return productRepository.addFirestore(product, photoUri)
    }

    fun edit() : LiveData<Boolean>{
        //productRepository.updateLocal(product)
        return productRepository.updateFirestore(product)
    }
}