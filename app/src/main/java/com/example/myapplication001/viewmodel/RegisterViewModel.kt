package com.example.myapplication001.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.myapplication001.model.entities.RegisterUser
import com.example.myapplication001.model.entities.User
import com.example.myapplication001.model.repository.UserRepository

class RegisterViewModel:ViewModel() {
    var user: User = User("", "", "", "","")
    private val userRepository: UserRepository = UserRepository()
    //var password:String=""

    fun singUp(): LiveData<User?> {
        return  userRepository.signUp(user, user.password)
    }
}