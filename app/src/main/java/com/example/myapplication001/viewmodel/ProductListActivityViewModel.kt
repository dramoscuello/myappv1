package com.example.myapplication001.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.example.myapplication001.model.entities.Product
import com.example.myapplication001.model.repository.ProductRepository

class ProductListActivityViewModel(application: Application):AndroidViewModel(application) {
    private val productRepository:ProductRepository = ProductRepository(application)
    var products: LiveData<List<Product>> = productRepository.productsObserver

    fun deleteProduct(myProduct: Product): LiveData<Boolean>{
        //productRepository.deleteLocal((myProduct))
        //productRepository.deleteFirestore(myProduct)
        //loadProducts()
        return productRepository.deleteFirestore(myProduct)
    }

    fun loadFakeData(){
        productRepository.loadFakeData()
    }

    fun loadProducts() {
        //productRepository.loadAllLocal()
        productRepository.loadAllFirestore()
        //productRepository.listenAllFirestore()
        //productRepository.loadAllAPI()

    }


    /*fun refreshData(){
        loadProducts()
        adapter.refresh(products)
    }*/



}