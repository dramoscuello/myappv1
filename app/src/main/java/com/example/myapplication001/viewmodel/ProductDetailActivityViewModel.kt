package com.example.myapplication001.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.example.myapplication001.model.entities.Product
import com.example.myapplication001.model.repository.ProductRepository

class ProductDetailActivityViewModel(application: Application) :AndroidViewModel(application) {

    private val productRepository:ProductRepository = ProductRepository(application)

    //var product = Product(name="",price = 0,)

    lateinit var product:LiveData<Product>

    fun getProductByKey(myProductKey: Int) {
        productRepository.getByKeyLocal(myProductKey)
        product = productRepository.productObserver
    }

    fun getProductById(myProductId: String) {
        productRepository.getByIdFirestore(myProductId)
        product = productRepository.productObserver
    }


}