package com.example.myapplication001.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.myapplication001.model.entities.User
import com.example.myapplication001.model.repository.UserRepository

class MainActivityViewModel:ViewModel() {
    var user: User = User("","","","","")
    var password:String=""
    private val userRepository: UserRepository = UserRepository()

    fun login(): LiveData<User?> {
        println(user.email)
        println(user.password)
        println("---------------------------")
        println(password)
        println("---------------------------")
        return userRepository.login(user.email, user.password)
    }

}