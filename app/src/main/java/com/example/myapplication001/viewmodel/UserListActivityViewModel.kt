package com.example.myapplication001.viewmodel

import androidx.lifecycle.ViewModel
import com.example.myapplication001.model.entities.RegisterUser
import com.example.myapplication001.view.UserAdapter

class UserListActivityViewModel:ViewModel() {
    val Users:ArrayList<RegisterUser> = arrayListOf()
    val adapter: UserAdapter = UserAdapter(Users)


    fun loadUsers():Unit{

        //Apply sirve para que todas las funciones dentro de los corchetes seapliquen al objeto sin necesidad de repetir
        Users.apply {
            clear()
            add(RegisterUser("1063162459","Deimer","deimer@correo.com","NO"))
            add(RegisterUser("1064978558","Yulieth","yulieth@correo.com","NO"))
            add(RegisterUser("30657287","Cecy","cecy@correo.com","NO"))
            add(RegisterUser("1063455678","Luka","luka@correo.com","NO"))
        }
    }

    fun refreshData(){
        adapter.refresh(Users)
    }
}